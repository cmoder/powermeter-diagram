Powermeter Diagram
==================

Description
-----------
This tool can display output from bicycle power meters as an interactive diagram
that can be viewed in the browser. It displays not only the pedaling power,
but computes from velocity and mass also e.g. the air drag and rolling resistance
(if the correct parameters are supplied).

It consists of two parts:

* Preprocessing: Together with GPSBabel, a Perl script extracts the relevant
  data fields, performs some smoothing and gap filling, and writes the
  output into a JSON object.

* Diagram: The JSON file is loaded by a HTML file, which displays the diagram
  using the Charts.js toolkit. The calculations of e.g. air drag and energy
  balance are performed here, so parameter values can be changed interactively
  without reloading the input data.

Requirements and Installation
-----------------------------
The following software packages are required:

* GPSBabel (https://gpsbabel.org/)
* Perl, with the following modules:
    * JSON
    * Time::Piece
    * Time::Seconds

The following include files are required:
* *chart.min.js* (from https://chartjs.org/), version 3.x
* *leaflet.css* (from https://leafletjs.com/)
* *leaflet.js* (from https://leafletjs.com/)
* *images/marker-icon.png* and *images/marker-shadow.png* (from https://leafletjs.com/)

Usage
-----
Convert the GPS data file into a JavaScript file that can be included by the
website:
```sh
$ GPSBabel -t -i garmin_fit -f input.fit -o unicsv -F - | gps_unicsv2chartjs.pl > data.js
````
Supported data formats depend only on GPSBabel; however, the only relevant formats
seem to be FIT and TCX. For reading the FIT 2.0 format, a reasonably new version of
GPSBabel is required.

Then, open the website with your favorite browser.

License
-------
CC-BY-SA: https://creativecommons.org/licenses/by-sa/4.0/
