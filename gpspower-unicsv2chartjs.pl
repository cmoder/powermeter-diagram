#!/usr/bin/perl -l

# DESCRIPTION:
# Reads a GPSBabel UniCSV file from Garmin FIT or TCX input (i.e. data every second,
# with speed, power and heart rate, and writes a website with a Chart.js diagram.
# Requires Chart.bundle.min.js in the same directory as the HTML file.

# USAGE:
# GPSBabel -t -i garmin_fit -f input.fit -o unicsv -F - | gps_unicsv2chartjs.pl > output.js

use warnings FATAL => 'all';
use strict;
use utf8;	# allow UTF-8 strings
use JSON;
use Time::Piece;
use Time::Seconds;
use constant PI => atan2( 0, -1 );
use constant P_HRV_WINDOW => 120;
use constant P_HRV_SHIFT => 10;
use constant ALPHA => 0.02;
use constant DEFAULT_ELE => 250;

sub deg2rad { $_[0] * PI / 180 }

sub min { $_[0] < $_[1] ? $_[0] : $_[1] }

sub max { $_[0] > $_[1] ? $_[0] : $_[1] }

sub def_default { $_[0] ? $_[0] : $_[1] }

sub average { my $a = 0; $a += ($_ ? $_ : 0) for @_; @_ ? $a / @_ : 0 }

sub exp_smooth {
	my @d = @_;
	my $sum = 0;
	my $corr = 0;
	
	# correction factor, accounts for limited window size => sum is still 1
	$corr += ALPHA * (1 - ALPHA)**$_ for(0..$#d);
	
	$sum += ALPHA/$corr * (1 - ALPHA)**$_ * $d[$#d - $_] for (0..$#d);
	
	return $sum;
}

# average (weighted, if elements are missing) of two exponential smoothings to both sides
sub exp_smooth_bidir {
	((@{$_[0]} + @{$_[1]}) ? ((@{$_[0]} * exp_smooth( @{$_[0]} ) + @{$_[1]} * exp_smooth( reverse @{$_[1]} ) ) / (@{$_[0]} + @{$_[1]})) : undef);
}

my (@track, %fields, $prev_lat, $prev_lon, $start_t, $prev_t, $tot_dist, $default_ele);
my %precision = ( 'time' => '%.0f', 'dist' => '%.3f', 'ele' => '%.2f', 'ele_smooth' => '%.2f', 'speed' => '%.1f', 'hr' => '%.1f', 'P_muscle' => '%.1f', 'P_muscle_smooth_hr' => '%.1f', 'slope' => '%.2f', 'temp' => '%.1f' );

# read file; columns: e.g. No,Latitude,Longitude,Altitude,Temperature,Speed,Heartrate,Cadence,Power,Date,Time
while (<>)
{
	# save column titles, since columns can be missing => order is not clear
	my $i = 0;
	/^No/ and (%fields = map { $_ => $i++ } split /[,\v]/), next;
	
	# save columns to variables, clean whitespace, set empty columns to undef, convert others to number (by adding 0)
	my @input = map { s/\s+//g; /^$/ ? undef : /^[\d.-]+$/ ? $_ + 0 : $_ } split /[,\v]/;
	my ($lat, $lon, $ele, $temp, $speed, $hr, $power, $date, $time) = map { defined $fields{$_} ? $input[$fields{$_}] : undef } ('Latitude', 'Longitude', 'Altitude', 'Temperature', 'Speed', 'Heartrate', 'Power', 'Date', 'Time');
	
	# assign start values for time and position
	($prev_lat, $prev_lon) = ($lat, $lon) if !defined $prev_lat;
	$start_t = Time::Piece->strptime( "$date $time", '%Y/%m/%d %T' ) unless $start_t;
	
	# record first elevation, to be used as default value
	$default_ele ||= $ele if (defined $ele);
	
	# parse time
	my $t = Time::Piece->strptime( "$date $time", '%Y/%m/%d %T' );
	$prev_t ||= $start_t->epoch;
	my $dt = $t->epoch - $start_t->epoch;
	my $ddt = $t->epoch - $prev_t;
	
	# calculate distance from last point
	my $dist = 60 * 1.852 * sqrt( ($lat - $prev_lat)**2 + (cos(deg2rad(($lat+$prev_lat)/2))*($lon-$prev_lon))**2);

	# correct distance if it is 0, but the speed is not 0 => compute distance from speed instead of coordinates
	$dist = $speed * 1 / 1000 if ($dist == 0 and defined $speed and $speed > 2);

	# sum distance
	$tot_dist += $dist;

	# save data in array
	push( @track, {
		'time' => $dt,
		'dx' => $dist,
		'dist' => $tot_dist,
		'ele' => ($ele and $ele != 0) ? $ele : undef,
		'speed' => defined $speed ? $speed * 3.6 : undef,
		'P_muscle' => $power || 0,
		'hr' => $hr,
		'temp' => $temp,
		'lat' => $lat,
		'lon' => $lon
	} );
	
	# store previous values, for computing differences
	($prev_lat, $prev_lon) = ($lat, $lon);
	$prev_t = $dt;
}

# set default elevation, if still not defined
$default_ele ||= DEFAULT_ELE;

for my $i (0..$#track)
{
	# average distance over five seconds (symmetric)
	$track[$i]->{'dist_smooth'} = average( map { $track[$_]->{'dx'} } (max($i-2, 0)..min($i+2, $#track)) ) + ($i > 0 ? $track[$i-1]->{'dist_smooth'} : 0);
	
	# exponential smoothing of past power, for calculation of heart rate
	$track[$i]->{'P_muscle_smooth_hr'} = exp_smooth( map { $track[$_]->{'P_muscle'} || 0 } (max($i-P_HRV_WINDOW-P_HRV_SHIFT, 0)..min($i-P_HRV_SHIFT, $#track)) );
	
	# bidirectional smoothing of elevation over 5 seconds
	$track[$i]->{'ele_smooth'} = exp_smooth_bidir(
		[ grep { defined $_ } map { $track[$_]->{'ele'} } (max($i-5, 0)..min($i, $#track))],
		[ grep { defined $_ } map { $track[$_]->{'ele'} } (max($i, 0)..min($i+5, $#track))]
	);
	
	$track[$i]->{'ele_smooth_filled'} = $track[$i]->{'ele_smooth'} || $default_ele;
	$default_ele = $track[$i]->{'ele_smooth'} if defined $track[$i]->{'ele_smooth'};
}

for my $i (0..$#track)
{
	$track[$i]->{'slope'} = ($track[min($i+5, $#track)]->{'dist'} - $track[max($i-5, 0)]->{'dist'}) > 0
		?
			($track[min($i+5, $#track)]->{'ele_smooth_filled'} - $track[max($i-5, 0)]->{'ele_smooth_filled'})
			/ ($track[min($i+5, $#track)]->{'dist'} - $track[max($i-5, 0)]->{'dist'}) / 10
		:
			0;

}

# discard standstill, by creating a hash with distances as keys; first element = labels, second element = hash with data
my @track_moving = sort { $a->[0] <=> $b->[0] } values %{ { map { my $l = sprintf( $precision{'dist'},  $_->{'dist'} ); $l => [$l, $_] } @track } };

# create output JSON hash:
# x axis
my $output->{'labels'} = [ map { $_->[0] } @track_moving ];

# y axes
$output->{'in_datasets'} = [
	map {
		my $t = $_;
		
		[ map { $_->{$t} ? 0 + sprintf($precision{$t}, $_->{$t}) : $_->{$t} } map { $_->[1] } @track_moving ]

	} ('ele_smooth', 'speed', 'hr', 'temp', 'slope', 'P_muscle', 'P_muscle_smooth_hr') ];

$output->{'pos'} = [ map { [ map { 0 + sprintf "%.7f", $_ } ($_->[1]{'lat'}, $_->[1]{'lon'}) ] } @track_moving ];

# convert test data to JSON object and write
print 'var data = ' . to_json($output,
	{'utf8' => 1, 'pretty' => 0, 'indent' => 0, 'space_after' => 0}) . ";";

