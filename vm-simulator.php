<!DOCTYPE html>
<html lang="de">
<head>
	<title>Velomobil-Simulator</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="de" />
	<meta name="author" content="Christoph Moder" />
	<meta name="description" content="Berechnung der Fahrwiderstände aus Leistung und Topographie" />
	<meta name="keywords" lang="de" content="Fahrrad, Velomobil, Luftwiderstand, Rollwiderstand, Leistungsmesser, Powermeter" />
	<meta name="keywords" lang="en" content="bike, bicycle, velomobile, air drag, rolling resistance, powermeter" />
	<meta name="robots" content="noindex, nofollow" />
	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<meta name="HandheldFriendly" content="true" />

	<link rel='icon' type='image/ico' href='/favicon.ico' />
	<link rel="stylesheet" type="text/css" href="../tracker/leaflet/leaflet.css" />
	<style>
		body			{ width: 100%; height: 100%; margin: 0; padding: 0; overflow-y: hidden; }
		#chartjsInfo		{ position: fixed; margin: 0.5em; font-size: inherit; white-space: nowrap; width: calc(100% - 350px); pointer-events: none; }	/* width: window width - width of map in chartjsParams = 300px */
		#chartjsInfo > *	{ display: inline-block; margin: 0; padding: 0; background-color: rgba(200, 200, 200, 0.8); pointer-events: auto; }
		#chartjsLegend		{ vertical-align: top; }
		#chartjsLegend ul	{ margin: 0; padding: 0; }
		#chartjsLegend li	{ list-style: none; float: left; padding: 0.5em; cursor: pointer; }
		#chartjsLegend li span:first-child	{ width: 1.5em; height: 1em; display:inline-block; margin: 0 0.5em 0 0.5em; vertical-align: middle; }
		#chartjsParams input	{ width: 4em; }
		table, tr		{ margin: 0; padding: 0; border-spacing: 0; }
		th			{ text-align: center; padding: 0.5em; }
		th > a			{ text-decoration: none; }
		td			{ padding: 0.2em; }
		input			{ text-align: right; }
		.transition		{ transition: all 1s ease; }	/* does not work, since display cannot be animated */
		.collapse,
		#show:not(:target) #do_hide,
		#show:target #do_show	{ display: none; }
		#do_show		{ display: inline; }
		#show:target .collapse	{ display: inherit; }

		/* spinner: outer div = rounded rectangle, semi-tranparent, fixed and centered; inner div = spinner, rectangle rounded to circle, dotted border, total width = width + 2 * margin + 2 * border width */
		#spinner		{ position: fixed; width: 20vh; height: 20vh; left: 50%; top: 50%; margin-left: -10vh; margin-top: -10vh; border-radius: 20%; background-color: rgba(220, 220, 220, 0.8); display: none; }
		#spinner > div		{ width: 12vh; height: 12vh; left: 50%; top: 50%; margin-left: 2vh; margin-top: 2vh; border: 2vh dotted grey; border-radius: 50%; animation: spin 1s linear infinite; }
		@keyframes spin		{ to { transform: rotate(360deg); } }
	</style>
</head>

<body>
<div id="chartjsInfo">
	<div id="chartjsParams"><form><table id="show" class="transition">
		<tr><th colspan="4" title="einblenden/ausblenden"><a href="#show" id="do_show" onClick="set_URL_parameters(); setTimeout(function(){ map.invalidateSize(); map_updated = true; }, 400);">[+]</a><a href="#" id="do_hide">[-]</a> Parameter/Statistik</th></tr>
		<tr class="collapse">
			<td title="Luftwiderstandskoeffizient * Querschnittsfläche = effektive Querschnittsfläche">cWA:</td><td><input type="number" min="0" step="0.005" id="cwa"></input>&nbsp;m<sup>2</sup></td>
			<td id="sum1_P_roll"></td><td></td>
		</tr>
		<tr class="collapse">
			<td><select id="input_roll_name">
				<option value="cr" data-title="Rollwiderstandskoeffizient" data-unit="" data-min="0" data-max="10" data-step="0.001" selected>cR</option>
				<option value="crdyn" data-title="dynamischer Rollwiderstandskoeffizient" data-unit="" data-min="0" data-max="10" data-step="0.001">cRdyn</option>
			</select></td><td><input type="number" id="input_roll_val"></input><span id="input_roll_unit"></span></td>
			<td id="sum1_P_air"></td><td></td>
		</tr>
		<tr class="collapse">
			<td><select id="input_mass_name">
				<option value="mrider" data-title="Masse des Fahrers" data-unit="kg" data-min="0" data-max="200" data-step="0.1" selected>m_rider</option>
				<option value="mbike" data-title="Masse des Fahrrads (incl. Räder)" data-unit="kg" data-min="0" data-max="100" data-step="0.1">m_bike</option>
				<option value="mwheels" data-title="rotierende Masse (Masse aller Räder)" data-unit="kg" data-min="0" data-max="100" data-step="0.1">m_wheels</option>
			</select></td><td><input type="number" id="input_mass_val"></input><span id="input_mass_unit"></span></td>
			<td id="sum1_P_uphill"></td><td></td>
		</tr>
		<tr class="collapse">
			<td><select id="input_other_name">
				<!-- <option value="distance" data-title="Streckenlänge" data-unit="km" data-min="0" data-max="1000" data-step="1" selected>distance</option> -->
				<option value="dtr" data-title="Verluste im Antriebsstrang (bezogen auf Tretleistung)" data-unit="%" data-min="0" data-max="100" data-step="1" selected>drivetrain</option>
				<option value="base_ele" data-title="Höhe des tiefsten Punkts" data-unit="m" data-min="-300" data-max="8000" data-step="0.1">base_ele</option>
				<option value="top_ele" data-title="Höhe des höchsten Punkts" data-unit="m" data-min="-300" data-max="8000" data-step="0.1">top_ele</option>
				<option value="temp" data-title="Temperatur" data-unit="°C" data-min="-30" data-max="50" data-step="0.1">temp</option>
				<option value="csv" data-title="Daten-Export:&#10;1 = CSV in Konsole&#10;2 = Tabelle in Konsole&#10;3 = Tabelle im Fenster&#10;4 = CSV im Fenster" data-unit="" data-min="0" data-max="4" data-step="1">CSV</option>
			</select></td><td><input type="number" id="input_other_val"></input><span id="input_other_unit"></span></td>
			<td id="sum1_P_brake"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="Durchschnitts-Tretleistung [W] (mit 0)">P_avg</td><td><span id="avg_muscle"></span>&nbsp;W</td>
			<td id="sum2_P_muscle"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="Normalized Power [W]">NP</td><td><span id="np"></span>&nbsp;W</td>
			<td id="sum2_P_down"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="Durchschnittsgeschwindigkeit [km/h]">v_avg</td><td><span id="avg_speed"></span>&nbsp;km/h</td>
			<td id="sum3_P_roll"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="Fahrzeit [min:s]">time</td><td><span id="t_net"></span></td>
			<td id="sum3_P_air"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="Höhenmeter [m]">ele_total</td><td><span id="ele_total"></span>&nbsp;m</td>
			<td id="sum3_P_uphill"></td><td></td>
		</tr>
		<tr class="collapse">
			<td title="verbrauchte Energie, Tretleistung + bergab [kJ]">energy (ped/pot)</td><td><span id="energy"></span>&nbsp;kJ</td>
			<td id="sum3_P_accel"></td><td></td>
		</tr>
		<tr class="collapse">
			<td></td><td></td>
			<td title="Verhältnis hineingesteckte Energie (netto bergab + Netto-Tretleistung, d.h. ohne Antriebsverluste) zu verbrauchter Energie; sollte 100% sein">in/out:</td><td><span id="in_out"></span>%</td>
		</tr>
		<tr class="collapse" id="mapcontainer"><td colspan="4" id="map" style="height: 300px;"></td></tr>
	</table></form>
	</div>
	<div id="chartjsLegend"></div>
</div>
<div id="spinner"><div></div></div>
<div id="wrapper">
	<canvas id="chart_canvas"><p>Error: JavaScript and HTML5 canvas required. Check if JavaScript is enabled.</p></canvas>
</div>

<script src="../tracker/leaflet/leaflet.js" type="text/javascript"></script>
<script src="chart.min.js"></script>
<script src="chartjs-plugin-dragdata.min.js"></script>
<script>

// dataset constants: IN_ variant must follow the corresponding constant
const ELE = 0, IN_ELE = 1, SPEED = 2, MUSCLE = 3, IN_MUSCLE = 4, DOWN = 5, DECEL = 6, ROLL = 7, AIR = 8, UP = 9, ACCEL = 10, BRAKE = 11, SPEED_MAX = 12, IN_SPEED_MAX = 13, WIND = 14, IN_WIND = 15, SLOPE = 16;
const DATASET_INPUT = [IN_ELE, IN_MUSCLE, IN_WIND, IN_SPEED_MAX];
const DATASET_INPUT_INTERPOLATED = [ELE, MUSCLE, WIND, SPEED_MAX];
const MAX_CANVAS_WIDTH = 5000;	// in pixels
const MAX_ITERATIONS = 1000;	// the iteration is stopped after this many iterations, even if it has not converged
const SPEED_INC = 0.1;	// in km/h; maximum adjustment to SPEED_CALC in each iteration
const MIN_SPEED = 0.1;	// in km/h; minimum allowed speed
const INITIAL_SPEED = 3;	// in km/h; start iteration with this minimum speed (= produces downhill power and some kinetic energy)
const MIN_AVG_SPEED = 1;	// in km/h; minimum average speed to cover the distance
const MIN_RESID_POWER = 0.1;	// in W; stop iteration when this residual power has been reached

// parameters that can be set via URL (and some via input field), with their default values
const DEFAULT_PARAMS = {
	'cwa': 0.05,	// air drag coefficient []
	'cr': 0.005,	// rolling resistance coefficient []
	'crdyn': 0.1,	// dynamic rolling resistance coefficient []
	'mrider': 75,	// mass of rider [kg]
	'mbike': 30,	// mass of bike [kg]
	'mwheels': 3,	// mass of the wheels [kg]
	'dtr': 8,	// drivetrain losses [%]
	'base_ele': 250,	// elevation of lowest point [m]
	'top_ele': 550,	// elevation of highest point [m]
	'start_ele': 0,	// elevation of starting point above base_ele [m]
	'distance': 10,	// distance [km]
	'temp': 20,	// temperature [°C]
	'default_power': 200,	// default muscle power [W], must be > 0
	'default_vmax': 100,	// default maximum speed [km/h], must be > 0
	'zoom_min': null,	// initial left border of diagram [px]
	'zoom_max': null,	// initial right border of diagram [px]
	'debug': 0,	// debug level
	'csv': 0	// write CSV data to console
};

// technical variables
var myChart;
var isDragging = false;
var map, marker, map_updated = false;
var pinch_start, pinch_end;
var swipe_x_start, swipe_x_end;
var spinner;

// diagram state
var data = [];
var params = Object.assign({}, DEFAULT_PARAMS);
var legend_bg_colors = [];
var xrange, max_xrange;
var canvas_width;

// statistics
var avg_speed;
var avg_speed_calc;
var avg_muscle;
var ele_total;
var np;
var t_net;
var sum_in = [], sum_out = [];
var sum_inst = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// limit index to range between min and max
function to_range(i, min, max) {
	return Math.max(Math.min(i, max), min);
}

// return a value from an array, care for index out of range, return default value if null
function get_val(n, i, def) {
	let d = data.datasets[n].data[to_range(i, 0, data.datasets[n].data.length-1)].y;
	return (d !== null ? d : def);
}

// write a debug message to the console log if the debug level is at least level
function debug_log(message, level) {
	if (params['debug'] >= level)
		console.log('DEBUG ' + level + ': ', message);
}

// get finger distance of two finger touch event; "pageX/pageY" refers to whole page, "clientX/clientY" only to screen size
function pinch_dist(e) {
	return Math.hypot(e.touches[0].clientX - e.touches[1].clientX, e.touches[0].clientY - e.touches[1].clientY);
}

// execute a long running function, and show a spinner animation
function show_spinner(f) {

	// switch spinner on
	spinner.display = 'block';

	// start the expensive function a bit later
	setTimeout(function() {
		f();
		spinner.display = 'none';
	}, 10);
}

// set some line properties in one single function call
function set_graph_color(dataset, color, filled = false, dashed = []) {
	data.datasets[dataset].borderColor = color;
	data.datasets[dataset].borderWidth = filled ? 1 : 4;
	data.datasets[dataset].fill = filled;
	data.datasets[dataset].borderDash = dashed;
}

// export all modified settings into URL parameters, so they can be retrieved by using the modified URL
function set_URL_parameters() {
	// generate parameters from current URL
	let urlparams = new URL(window.location).searchParams;

	// copy diagram parameter into URL parameter; but only those that differ from their defaults
	Object.keys(params).filter( k => params[k] !== DEFAULT_PARAMS[k] && params[k] !== null ).forEach( v => urlparams.set(v, params[v]) );

	// copy visibility of datasets into URL parameter
	data.datasets.forEach( v => v.hidden ? urlparams.set(v.label, 0) : urlparams.delete(v.label) );

	// replace URL
	history.replaceState('foobar', document.title, '?' + urlparams.toString() + location.hash);
}

// write a fraction as whole percent
function format_percent(x) {
	return Math.round(x * 100);
}

// round a number to the given number of digits
function round_to(x, digits) {
	return Math.round( x * 10**digits ) / 10**digits;
}

// write time (in seconds) as e.g. "3h 24:05"
function format_time(t) {
	return (t >= 3600 ? Math.floor(t / 3600) + 'h' : '') + Math.floor((t % 3600) / 60) + ':' + (t % 60).toString().replace(/^(\d)$/, "0$1");
}

// get interpolated value from dataset at position x
function interpolate_data(dataset, x) {

	// find the two neighboring elements
	[v1, v2] = data.datasets[dataset].data.filter( (v, i, a) => (x >= a[Math.max(i-1, 0)].x && x <= a[i].x) || (x >= a[i].x && x <= a[Math.min(i+1, data.datasets[dataset].data.length-1)].x));

	// interpolate between them
	return (x - v1.x) / (v2.x - v1.x) * v2.y + (v2.x - x) / (v2.x - v1.x) * v1.y;
}

// compute the moving average from -average to +average around each data point
function average_dataset(input, average) {

	let output = [];

	for (let i = 0; i < input.length; i++) {

		let avg = 0;
		let k = 0;

		for (let j = to_range(i - average, 0, input.length-1); j <= to_range(i + average, 0, input.length-1); j++ ) {
			k++;
			avg += input[j].y;
		}

		output[i] = k > 0 ? avg / k : null;
	}

	return output;
}

// update the statistics table
function update_legend() {

	[ROLL, AIR, UP, BRAKE].forEach( (v,i) => document.getElementById('pc1_' + v).innerHTML = format_percent( sum_out[v] / [ROLL, AIR, UP, BRAKE].map(i => sum_out[i]).reduce((t, v) => t + v) ) );
	[ROLL, AIR, UP, BRAKE].forEach( (v,i) => document.getElementById('pc1_' + v).title = sum_out[v] + ' J' );

	[MUSCLE, DOWN].forEach( (v,i) => document.getElementById('pc2_' + v).innerHTML = format_percent( sum_in[v] / [MUSCLE, DOWN].map(i => sum_in[i]).reduce((t, v) => t + v) ) );
	[MUSCLE, DOWN].forEach( (v,i) => document.getElementById('pc2_' + v).title = sum_in[v] + ' J' );

	[ROLL, AIR, UP, ACCEL].forEach( (v,i) => document.getElementById('pc3_' + v).innerHTML = format_percent( sum_inst[v] / [ROLL, AIR, UP, ACCEL].map(i => sum_inst[i]).reduce((t, v) => t + v) ) );

	document.getElementById('in_out').innerHTML = format_percent( [MUSCLE, DOWN].map(i => sum_in[i]).reduce((t, v) => t + v) / [ROLL, AIR, UP, BRAKE].map(i => sum_out[i]).reduce((t, v) => t + v) );

	document.getElementById('avg_muscle').innerHTML = round_to(avg_muscle, 0);
	document.getElementById('avg_muscle').title = avg_muscle + ' W';
	document.getElementById('np').innerHTML = round_to(np, 0);
	document.getElementById('np').title = np + ' W';
	document.getElementById('avg_speed').innerHTML = round_to(avg_speed, 1);
	document.getElementById('avg_speed').title = avg_speed + ' km/h';
	document.getElementById('t_net').innerHTML = format_time(t_net);
	document.getElementById('ele_total').innerHTML = round_to(ele_total, 0);
	document.getElementById('ele_total').title = ele_total + ' m';
	let potential_energy = Math.max((params['mrider'] + params['mbike']) * 9.81 * (get_val(ELE, 0, 0) - get_val(ELE, data.datasets[ELE].data.length, 0)), 0);
	document.getElementById('energy').innerHTML = round_to(avg_muscle * t_net / 1000, 0) + ' / ' + round_to(potential_energy / 1000, 0);
	document.getElementById('energy').title = (avg_muscle * t_net) / 1000 + ' / ' + potential_energy / 1000 + ' kJ';
}

function compute_power(speed, prev_speed, x, delta_x, ele, weight, wind, delta_h, slope) {

	// rolling resistance: P = c_r * v * normal force (= m * g * cos(slope)) + dynamic rolling resistance
	let P_roll = (speed / 3.6) * Math.cos(Math.atan2(slope, 1)) * (weight * params['cr'] + (speed / 3.6) * params['crdyn']);

	// air resistance: P = F * v, with F = cWA * air density (from barometric formula) * (v + v_wind)^2
	let P_air = (speed / 3.6) * Math.pow((speed + wind) / 3.6, 2) * (176.5 * Math.exp(-ele * .0001253) * params['cwa'] / (273 + params['temp']));

	// slope force: P = m * g * deltaH
	let P_slope = weight * delta_h;
	let P_up = P_slope > 0 ? P_slope : 0;
	let P_down = P_slope < 0 ? (-P_slope) : 0;

	// acceleration power, from changes in speed: P = F_a * v = m*a * v = m * dv/dt * v = m * dv * v;
	// mass = m_bike + m_rider; since rotational energy is part of the kinetic energy, add also the mass of the wheels
	let last_speed = (speed + prev_speed) / 2;
	let P_a = (params['mbike'] + params['mrider'] + params['mwheels']) * (speed - prev_speed) * last_speed / Math.pow(3.6, 2);
	let P_accel = P_a > 0 ? P_a : 0;
	let P_decel = P_a < 0 ? -P_a : 0;

	return [P_roll, P_air, P_up, P_down, P_accel, P_decel];
}

// compute the statistics and show them in the parameter window on the left
function compute_summary() {

	// slope = difference of uphill and downhill; negative if more downhill than uphill
	sum_slope = data.datasets[UP].data.reduce( (total, val) => total + val.y, 0 ) - data.datasets[DOWN].data.reduce( (total, val) => total + val.y, 0 );

	// output power
	sum_out[ROLL] = data.datasets[ROLL].data.reduce( (total, val) => total + val.y, 0 );
	sum_out[AIR] = data.datasets[AIR].data.reduce( (total, val) => total + val.y, 0 );
	sum_out[UP] = Math.max(sum_slope, 0);
	sum_out[BRAKE] = data.datasets[BRAKE].data.reduce( (total, val) => total + val.y, 0 );

	// input power: muscle and downhill
	// muscle power: adjust by drive train efficiency; this is the amount of power additionally needed => divide by this to get the residual power
	sum_in[MUSCLE] = data.datasets[MUSCLE].data.reduce( (total, val) => total + val.y, 0 ) * (1 - params['dtr'] / 100);
	sum_in[DOWN] = -Math.min(sum_slope, 0);

	// sum up energy in each moment
	sum_inst[UP] = 0;
	sum_inst[ACCEL] = 0;
	sum_inst[AIR] = 0;
	sum_inst[ROLL] = 0;

	for (let i = 0; i < data.datasets[SPEED].data.length; i++) {

		// residual power not from muscles = (downhill + deceleration) - (uphill + acceleration)
		let residual = Math.max( (get_val(DOWN, i, 0) + get_val(DECEL, i, 0)) - (get_val(UP, i, 0) + get_val(ACCEL, i, 0)), 0 );

		// uphill by muscle = uphill - deceleration
		sum_inst[UP] += Math.max(get_val(UP, i, 0) - get_val(DECEL, i, 0), 0);

		// acceleration by muscle = acceleration - downhill
		sum_inst[ACCEL] += Math.max(get_val(ACCEL, i, 0) - get_val(DOWN, i, 0), 0);

		// air resistance by muscle = air resistance - residual power
		sum_inst[AIR] += Math.max( get_val(AIR, i, 0) - residual, 0);
		residual = Math.max(residual - get_val(AIR, i, 0), 0);

		// rolling resistance by muscle = rolling resistance - residual power not consumed by air drag
		sum_inst[ROLL] += Math.max(get_val(ROLL, i, 0) - residual, 0);
	}

	// compute the remaining cumulative/average values
	avg_speed = data.datasets[SPEED].data.filter( val => val.y > 0 ).reduce( (total, val, index, arr) => total + val.y / arr.length, 0 );
	avg_muscle = data.datasets[MUSCLE].data.reduce( (total, val, index, arr) => total + val.y / arr.length, 0 );
	ele_total = data.datasets[ELE].data.reduce( (total, val, index, arr) => total + (index > 0 ? Math.max(arr[index].y - arr[index-1].y, 0) : 0), 0 );
	t_net = data.datasets[SPEED].data.length - 1;

	// compute normalized power:
	// * moving average over 30 s
	// * to the fourth power
	// * take fourth root of the average
	let np_array = average_dataset(data.datasets[MUSCLE].data, 15).map( v => Math.pow(v, 4) );
	np = Math.pow(np_array.reduce( (total, val, index, arr) => total + val / arr.length, 0 ), 1/4);
}

// do the actual computation; might be called also after loading, when parameters have been changed
function compute_diagram() {

	if (params['debug'] >= 1)
		console.time('compute');

	// clear arrays
	[ELE, MUSCLE, WIND, SPEED, SPEED_MAX, ROLL, AIR, UP, DOWN, ACCEL, DECEL, BRAKE, SLOPE].forEach( v => data.datasets[v].data = [] );

	// distance and speed at start
	let x = 0;
	let speed = 0, prev_speed = 0;
	let csv = [['t [s]', 'x [km]', 'ele [m]', 'slope [%]', 'wind [km/h]', 'speed_max [km/h]', 'speed [km/h]', 'P_muscle [W]', 'P_down [W]', 'P_decel [W]', 'P_roll [W]', 'P_air [W]', 'P_up [W]', 'P_accel [W]', 'P_brake [W]']];
	let log_output = null;

	// weight force of bike and rider
	let weight = (params['mbike'] + params['mrider']) * 9.81;

	// iterate over time; stop if maximum time is reached or distance is covered
	for (let t = 0; t <= params['distance'] / MIN_AVG_SPEED * 3600 && x <= params['distance']; t++) {

		let P_roll = null, P_air = null, P_up = null, P_down = null, P_accel = null, P_decel = null;
		let P_muscle = interpolate_data(IN_MUSCLE, x);
		let P_resid = Infinity;
		let prev_P_resid = Infinity;
		let ele = interpolate_data(IN_ELE, x);
		let speed_max = interpolate_data(IN_SPEED_MAX, x);
		let wind = interpolate_data(IN_WIND, x);
		let i;

		// we need some initial speed to get the computation going;
		// either to produce some downhill power if there is no muscle power,
		// or to make the computation more stable (inertia from kinetic energy)
		speed = to_range(speed, INITIAL_SPEED, speed);

		// distance per timestep (1 s) in m = convert speed from km/h to m/s
		let delta_x = speed / 3.6;

		// compute slope (from previous point)
		let delta_h = ele - interpolate_data(IN_ELE, to_range(x - delta_x / 1000, 0, x));
		let slope = delta_x === 0 ? 0 : delta_h / delta_x;

		// determine speed by comparing input and output power and adjusting the speed accordingly;
		// stop if the residual becomes larger again or if the maximum number of iterations has been reached
		for (i = 0; i < MAX_ITERATIONS && Math.abs(P_resid) <= Math.abs(prev_P_resid) && Math.abs(P_resid) > MIN_RESID_POWER && speed > MIN_SPEED; i++) {

			// save residual, to compare it with the next one
			prev_P_resid = P_resid;

			// compute the power
			[P_roll, P_air, P_up, P_down, P_accel, P_decel] = compute_power(speed, prev_speed, x, delta_x, ele, weight, wind, delta_h, slope);

			// compute output power sum
			P_out = P_roll + P_air + P_up + P_accel;

			// compute input power sum: muscle (minus drivetrain losses), downhill, deceleration
			P_in = P_muscle * (1 - params['dtr'] / 100) + P_down + P_decel;

			// difference between input and output power
			P_resid = P_in - P_out;

			// increase or decrease calculated speed, depending on the residual power
			// and the number of iterations (later = smaller changes);
			// do the next iteration with this updated speed
			speed += to_range(P_resid / 1000, -SPEED_INC / Math.log2(i), +SPEED_INC / Math.log2(i));

			// limit speed to maximum speed; and also prevent speed from falling to 0
			speed = to_range(speed, MIN_SPEED, speed_max);
		}

		// if the available energy could not be consumed, e.g. due to speed limit, reduce the muscle power
		if (P_resid > P_muscle) {
			P_resid -= P_muscle;
			P_muscle = 0;
		}
		else if (P_resid > 0) {
			P_muscle -= P_resid;
			P_resid = 0;
		}

		// brake power = residual power, but only if positive and if almost no muscle power
		data.datasets[BRAKE].data[t] = { 'x': x, 'y': to_range(P_resid, 0, P_resid) };

		// assign interpolated values to diagram
		data.datasets[ELE].data[t] = { 'x': x, 'y': ele };
		data.datasets[MUSCLE].data[t] = { 'x': x, 'y': P_muscle };
		data.datasets[SPEED_MAX].data[t] = { 'x': x, 'y': speed_max };
		data.datasets[WIND].data[t] = { 'x': x, 'y': wind };
		data.datasets[SLOPE].data[t] = { 'x': x, 'y': 100 * slope };

		// assign values to diagram
		data.datasets[SPEED].data[t] = { 'x': x, 'y': speed };
		data.datasets[ROLL].data[t] = { 'x': x, 'y': P_roll };
		data.datasets[AIR].data[t] = { 'x': x, 'y': P_air };
		data.datasets[UP].data[t] = { 'x': x, 'y': P_up };
		data.datasets[DOWN].data[t] = { 'x': x, 'y': P_down };
		data.datasets[ACCEL].data[t] = { 'x': x, 'y': P_accel };
		data.datasets[DECEL].data[t] = { 'x': x, 'y': P_decel };

		debug_log(['t:', t, 'x:', x, 'v:', speed, 'i:', i], 3);

		// if data output as CSV table is desired: add line
		if (params['csv'])
			csv.push([t, x, ele, slope, wind, speed_max, speed, P_muscle, P_down, P_decel, P_roll, P_air, P_up, P_accel, to_range(P_resid, 0, P_resid)]);

		// update the covered distance
		x += delta_x / 1000;

		// save the previous speed for next iteration
		prev_speed = speed;
	}

	// compute the statistics and show them in the parameter window on the left
	compute_summary();

	// if data output as CSV table is desired:
	switch (params['csv']) {
		// CSV in console: convert array to string and write it
		case 1:
			console.log(csv.map( v => v.join('\t') ).join('\n'));
			break;
		// table in console
		case 2:
			console.table(csv);
			break;
		// HTML table in new window
		case 3:
			log_output = '<html><body><table><tr>'
				+ csv[0].map( v => '<th>' + v + '</th>').join('') + '</tr>\n'
				+ csv.slice(1).map(v => '<tr>' + v.map(w => '<td>' + w + '</td>').join('') + '</tr>\n').join('')
				+ '</table></body></html>';
		// CSV in new window
		case 4:
			log_output = log_output || '<html><body><pre>' + csv.map( v => v.join('\t') ).join('\n') + '</pre></body></html>';

			// open new window and write data
			let log_window = window.open('', '');
			log_window.document.write(log_output);

			// reset the log type, so no new window is created; and if necessary, adjust the input field
			params['csv'] = 0;
			if (document.getElementById('input_other_name')[document.getElementById('input_other_name').selectedIndex].value === 'csv')
				document.getElementById('input_other_val').value = params['csv'];
			break;
	}

	// if debugging: write time required for diagram computation
	if (params['debug'] >= 1)
		console.timeEnd('compute');
}

function create_map() {
	map = L.map('map', {
		'attributionControl': false,
		'zoomAnimation': false,
		'fadeAnimation': false,
	}).addLayer(
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {'useCache': true})
	).setView([48, 12], 13);
	marker = L.marker([48, 12]).addTo(map);
}

// zoom by setting the minimum and maximum values of the x axis
function zoom_range(min, max) {

	// set new diagram endpoints
	myChart.options.scales['x'].min = to_range(min, 0, max_xrange);
	myChart.options.scales['x'].max = to_range(max, myChart.options.scales['x'].min + 0.1, max_xrange);

	// record them also in the parameters, and round them to fit the input form precision
	params['zoom_min'] = myChart.options.scales['x'].min > 0 ? round_to(myChart.options.scales['x'].min, 2) : null;
	params['zoom_max'] = myChart.options.scales['x'].max < max_xrange ? round_to(myChart.options.scales['x'].max, 2) : null;

	// update diagram, and write the changes into the URL parameters
	show_spinner(function() {
		myChart.update();
		set_URL_parameters();
	});
}

// zoom by resizing the canvas or adjusting the min/max values of the x axis
function zoom(factor) {

	// get the width of the diagram container (in px)
	let current_width = parseInt(document.getElementById('wrapper').style.width, 10);
	debug_log(['zoom: factor', factor, 'xrange', xrange, 'current_width', current_width, 'canvas_width', canvas_width], 1);

	// if the diagram is not cropped to a range (but goes from 0 to params['distance'])
	// and zooming out or container is already smaller than canvas width, then adjust container
	if (xrange === max_xrange && (factor > 1 || current_width < canvas_width)) {
		document.getElementById('wrapper').style.width = Math.round(to_range(current_width / factor, 500, canvas_width)) + 'px';
		myChart.resize();
	// otherwise: adjust diagram range, and take into account current scroll position within the current range
	} else {
		// scroll position (relative to current selection): 0 = left, 1 = right
		scroll = window.pageXOffset / (document.getElementById('chart_canvas').offsetWidth - window.innerWidth);

		// scroll position, taking into account the current selection
		scroll_zoomed = ((myChart.options.scales['x'].min || 0) + scroll * xrange) / max_xrange;

		// scale range of x axis; restrict range to 10 m or total length
		xrange = to_range(xrange * factor, 0.01, max_xrange);

		// adjust min/max values of x axis
		zoom_range(Math.round((max_xrange - xrange) * scroll_zoomed), max_xrange - Math.round((max_xrange - xrange) * (1 - scroll_zoomed)));
	}
}

// scroll by adding/subtracting a value to/from the minimum and maximum values of the x axis
function scroll_rel(scroll) {
	let min = to_range(myChart.options.scales['x'].min + scroll, 0, max_xrange - xrange);
	let max = min + xrange;
	zoom_range(min, max);
}

window.onload = function() {

	// switch on spinner, while initialization is running
	spinner = document.getElementById('spinner').style;
	spinner.display = 'block';

	// initialize metadata of datasets
	data.datasets = [];

	// first the input datasets that can be dragged:
	data.datasets[IN_ELE] = { 'data': [], 'label': 'in_ele', 'yAxisID': 'h', 'unit': 'm', 'description': 'Höhe über Basishöhe [m]', dragData: true };
	data.datasets[IN_MUSCLE] = { 'data': [], 'label': 'in_P_muscle', 'yAxisID': 'P_hidden', 'unit': 'W', 'description': 'Tretleistung [W]', dragData: true };
	data.datasets[IN_WIND] = { 'data': [], 'label': 'in_wind', 'yAxisID': 'v_wind', 'unit': 'km/h', 'description': 'Windgeschwindigkeit [km/h]', dragData: true };
	data.datasets[IN_SPEED_MAX] = { 'data': [], 'label': 'in_speed_max', 'yAxisID': 'v', 'unit': 'km/h', 'description': 'Tempolimit [km/h]', dragData: true };

	// then the interpolated and the auxiliary datasets that are not shown as curves, only in the tooltip and used in the computation;
	// however, when stacking, the curve must be shown => MUSCLE
	data.datasets[ELE] = { 'data': [], 'label': 'ele', 'yAxisID': 'h', 'unit': 'm', 'description': 'Höhe über Basishöhe [m]', dragData: false, showLine: false };
	data.datasets[MUSCLE] = { 'data': [], 'label': 'P_muscle', 'yAxisID': 'P_in', 'unit': 'W', 'description': 'Tretleistung [W]', dragData: false, showLine: true };
	data.datasets[WIND] = { 'data': [], 'label': 'wind', 'yAxisID': 'v_wind', 'unit': 'km/h', 'description': 'Windgeschwindigkeit [km/h]', dragData: false, showLine: false };
	data.datasets[SPEED_MAX] = { 'data': [], 'label': 'speed_max', 'yAxisID': 'v', 'unit': 'km/h', 'description': 'Tempolimit [km/h]', dragData: false, showLine: false };
	data.datasets[SLOPE] = { 'data': [], 'label': 'slope', 'yAxisID': 'v', 'unit': '%', 'description': 'Steigung [%]', dragData: false, showLine: false };

	// then the computed datasets
	data.datasets[SPEED] = { 'data': [], 'label': 'speed', 'yAxisID': 'v', 'unit': 'km/h', 'description': 'Geschwindigkeit [km/h]', dragData: false };
	data.datasets[DOWN] = { 'data': [], 'label': 'P_down', 'yAxisID': 'P_in', 'unit': 'W', 'description': 'Leistung aus Abwärtsfahrt [W]', dragData: false };
	data.datasets[DECEL] = { 'data': [], 'label': 'P_decel', 'yAxisID': 'P_in', 'unit': 'W', 'description': 'Ausrollen, Leistung aus Geschwindigkeitsverminderung [W]', dragData: false };
	data.datasets[ROLL] = { 'data': [], 'label': 'P_roll', 'yAxisID': 'P_out', 'unit': 'W', 'description': 'Rollwiderstand [W]', dragData: false };
	data.datasets[AIR] = { 'data': [], 'label': 'P_air', 'yAxisID': 'P_out', 'unit': 'W', 'description': 'Luftwiderstand [W]', dragData: false };
	data.datasets[UP] = { 'data': [], 'label': 'P_uphill', 'yAxisID': 'P_out', 'unit': 'W', 'description': 'Kletterarbeit [W]', dragData: false };
	data.datasets[ACCEL] = { 'data': [], 'label': 'P_accel', 'yAxisID': 'P_out', 'unit': 'W', 'description': 'Beschleunigungsarbeit [W]', dragData: false };
	data.datasets[BRAKE] = { 'data': [], 'label': 'P_brake', 'yAxisID': 'P_out', 'unit': 'W', 'description': 'Bremsleistung [W]', dragData: false };

	// process URL parameters: add them to params[], or set the visibility
	new URL(window.location).searchParams.forEach( function(v,k) {

		if (k in params)
			params[k] = Number(v);
		else if ((i = data.datasets.findIndex( e => e.label === k )) !== -1)
			data.datasets[i].hidden = ((v === 'false' || Number(v) === 0) ? true : false);
	} );

	// initialize elevation and maxspeed from either default values or from given data
<?php
const BRAKE_DECELERATION = 1;	# in m/s^2
const MAX_SPEED = 100;		# in km/h

const HIGHWAY_LEVEL = [
	'footway' => 1,
	'pedestrian' => 1,
	'living_street' => 2,
	'path' => 2,
	'track' => 2,
	'service' => 3,
	'residential' => 3,
	'unclassified' => 3,
	'cycleway' => 3,
	'tertiary_link' => 4,
	'tertiary' => 5,
	'secondary_link' => 5,
	'secondary' => 6,
	'primary_link' => 6,
	'primary' => 7,
	'trunk_link' => 7,
	'trunk' => 8,
	'motorway_link' => 8,
	'motorway' => 9,
];

# if it looks like a BRouter URL, extract the parameters
if (preg_match('/lonlats=[[:alnum:]&=.,;%_-]+$/', $_GET['brouter'], $matches)) {

	$url = "http://brouter.de/brouter?format=geojson&$matches[0]";

	if (!preg_match('/profile=\w+/', $brouter))
		$url .= '&profile=vm-forum-velomobil-schnell';

	if (!preg_match('/alternativeidx=\d+/', $brouter))
		$url .= '&alternativeidx=0';

	# download BRouter routing data as JSON
	$json = json_decode(file_get_contents(preg_replace('/;/', '%7c', $url), false));

	$track_length = $json->{'features'}[0]->{'properties'}->{'track-length'} / 1000;

	$prev_pos = array_slice($json->{'features'}[0]->{'geometry'}->{'coordinates'}[0], 0, 3);
	$prev_highway = 'motorway';	# start with the highest category
	$dist = 0;
	$prev_dist = -1;
	$next_speed = NULL;

	# iterate over messages; skip first line = header
	foreach (array_slice($json->{'features'}[0]->{'properties'}->{'messages'}, 1) as $message) {

		# extract position and elevation; use previous values, since each segment contains the values of the endpoint
		if ($dist > $prev_dist) {
			$ele[] = ['x' => $dist, 'y' => $prev_pos[2]];
			$pos[] = ['x' => $dist, 'y' => [$prev_pos[1], $prev_pos[0]]];
		}

		# set initial maximum speed; if no speed from the previous iteration is given, use MAX_SPEED
		$speed = $next_speed ?? MAX_SPEED;
		$next_speed = NULL;

		# check if we have to go around a tight corner => convert turncost to speed
		# turncost = (1 - cos(angle)) * 150 + 0.2
		# => attention: this applies to the whole segment; however, high values seem to appear only at junctions
		# 45°: turncost = 44; speed = 60 km/h
		# 90°: turncost = 150; speed = 30 km/h
		# 180°: turncost = 300; speed = 15 km/h
		# => parabolic fit: 0.00073 x^2 - 0.43 x + 78
		if ($message[6] > 40)
			$speed = min($speed, 0.00073 * $message[6]**2 - 0.43 * $message[6] + 78);

		# check if there is a roundabout; set speed to 30 km/h
		if (preg_match('/junction=roundabout/', $message[9]))
			$speed = min($speed, 30);

		# check if there is some traffic calming at the node
		if (preg_match('/traffic_calming=/', $message[10]))
			$speed = min($speed, 30);

		# check if there is some traffic calming at the whole segment
		if (preg_match('/traffic_calming=/', $message[9])) {
			$speed = min($speed, 30);
			$next_speed = 30;
		}

		# check if we have to give way
		if (preg_match('/highway=give_way/', $message[10]))
			$speed = min($speed, 20);

		# check if we are changing to a road with higher priority; then we also have to go very slowly
		if (preg_match('/highway=(\w+)/', $message[9], $match)
		    && $match[1] !== $prev_highway
		    && HIGHWAY_LEVEL[$match[1]] > HIGHWAY_LEVEL[$prev_highway]) {
			$speed = min($speed, 20);
			$prev_highway = $match[1];
		}

		# check if we have to stop
		# FIXME: give_way duplicate?
		if (preg_match('/highway=(stop|give_way)/', $message[10]))
			$speed = 0;

		# specify a maximum speed only if it is reduced
		if ($speed < MAX_SPEED) {

			# compute braking distance from MAX_SPEED to the new maximum speed
			# with v_f^2 = v_i^2 + 2 * a * x
			$full_braking_distance = ((MAX_SPEED / 3.6)**2 - ($speed / 3.6)**2) / (2 * BRAKE_DECELERATION * 1000);

			# search last entry
			$last_maxspeed = array_key_last($max_speed);

			# if there is one
			if ($last_maxspeed) {

				# assume the same acceleration as the braking deceleration
				$braking_distance = min($dist - $max_speed[$last_maxspeed]['x'], $full_braking_distance);

				# if the last maxspeed point is closer than the required braking distance
				if ($dist - $full_braking_distance <= $max_speed[$last_maxspeed]['x']) {

					# compute initial speed when braking from this last point
					$initial_speed = 3.6 * sqrt(($speed / 3.6)**2 + 2 * BRAKE_DECELERATION * ($braking_distance * 1000));

					# if the max speed in this point is higher, adjust it to the speed on the current braking curve
					$max_speed[$last_maxspeed]['y'] = min($max_speed[$last_maxspeed]['y'], $initial_speed);
				}
				# otherwise, simply append a new braking start point
				else
					$max_speed[] = ['x' => $dist - $full_braking_distance, 'y' => MAX_SPEED];
			}
			# if this is the first point in the maxspeed array
			else {
				$braking_distance = min($full_braking_distance, $dist);

				# compute initial speed when braking from this last point
				$initial_speed = 3.6 * sqrt(($speed / 3.6)**2 + 2 * BRAKE_DECELERATION * ($braking_distance * 1000));

				$max_speed[] = ['x' => $dist - $braking_distance, 'y' => $initial_speed];
			}

			# search last entry
			$last_maxspeed = array_key_last($max_speed);

			# add actual maxspeed point, if we are on a new position; otherwise adjust the previous point
			if ($dist > $max_speed[$last_maxspeed]['x'])
				$max_speed[] = ['x' => $dist, 'y' => $speed];
			else
				$max_speed[$last_maxspeed]['y'] = min($speed, $max_speed[$last_maxspeed]['y']);

			# add other side of brake curve only if we are not yet at the end of the track
			if ($dist < $track_length) {

				# assume the same acceleration as the braking deceleration
				$accel_distance = min($full_braking_distance, $track_length - $dist);

				# with v_f^2 = v_i^2 + 2 * a * x
				$final_speed = 3.6 * sqrt(($speed / 3.6)**2 + 2 * BRAKE_DECELERATION * ($accel_distance * 1000));

				$max_speed[] = ['x' => $dist + $accel_distance, 'y' => $final_speed];
			}
		}

		# set the position/distance for the next node
		$prev_dist = $dist;
		$dist += floatval($message[3]) / 1000;
		$prev_pos = [floatval($message[0]) / 1000000, floatval($message[1]) / 1000000, floatval($message[2])];
	}

	# compute the minimum elevation
	$base_ele = array_reduce($ele, function($min, $v) { return $v['y'] < $min ? $v['y'] : $min; }, INF);
	$top_ele = array_reduce($ele, function($max, $v) { return $v['y'] > $max ? $v['y'] : $max; }, -INF);

	// add last elements (= duplicate the previous), so there are entries also for the max x value
	if ($ele[array_key_last($ele)]{'x'} < $track_length)
		$ele[] = ['x' => $track_length, 'y' => $ele[array_key_last($ele)]{'y'}];

	if ($max_speed[array_key_last($max_speed)]{'x'} < $track_length)
		$max_speed[] = ['x' => $track_length, 'y' => $max_speed[array_key_last($max_speed)]{'y'}];

	if ($pos[array_key_last($pos)]{'x'} < $track_length)
		$pos[] = ['x' => $track_length, 'y' => $pos[array_key_last($pos)]{'y'}];

	// set parameters from BRouter route
	echo("params['distance'] = $track_length;\n");
	echo("params['base_ele'] = $base_ele;\n");
	echo("params['top_ele'] = $top_ele;\n");

	// fill data arrays from BRouter route
	echo("data.datasets[IN_ELE].data = " . json_encode($ele, JSON_NUMERIC_CHECK) . ";\n");
	echo("data.datasets[IN_SPEED_MAX].data = " . json_encode($max_speed, JSON_NUMERIC_CHECK) . ";\n");
	echo("data.pos = " . json_encode($pos, JSON_NUMERIC_CHECK) . ";\n");
} else {
?>
	data.datasets[IN_ELE].data = [ { x: 0, y: params['base_ele'] + params['start_ele'] }, { x: params['distance'], y: params['base_ele'] } ];
	data.datasets[IN_SPEED_MAX].data = [ { x: 0, y: params['default_vmax'] }, { x: params['distance'], y: params['default_vmax'] } ];
	document.getElementById('mapcontainer').style.display = 'none';
<?php } ?>


	// initialize input datasets from params[] (after URL parameters have been considered)
	data.datasets[IN_MUSCLE].data = [ { x: 0, y: params['default_power'] }, { x: params['distance'], y: params['default_power'] } ];
	data.datasets[IN_WIND].data = [ { x: 0, y: 0 }, { x: params['distance'], y: 0 } ];

	// initialize legend background colors
	[ELE, SPEED, SPEED_MAX, WIND, IN_ELE, IN_SPEED_MAX, IN_WIND].forEach( v => legend_bg_colors[v] = 'rgba(200,200,200,0.3)' );
	[DOWN, DECEL, MUSCLE, IN_MUSCLE].forEach( v => legend_bg_colors[v] = 'rgba(178,24,43,0.3)' );
	[ROLL, AIR, UP, ACCEL, BRAKE].forEach( v => legend_bg_colors[v] = 'rgba(33,102,172,0.3)' );

	// get max zoom range
	max_xrange = Number(params['distance']);
	xrange = max_xrange;

	// compute data and update summary, since the arrays must be filled before creating the diagram
	compute_diagram();

	// elevation
	set_graph_color(ELE, 'rgba(102,102,102,1)');
	set_graph_color(IN_ELE, 'rgba(102,102,102,1)');
	set_graph_color(SLOPE, 'rgba(102,102,102,1)');

	// speed
	set_graph_color(SPEED, 'blue');
	set_graph_color(WIND, 'darkturquoise');
	set_graph_color(IN_WIND, 'darkturquoise');
	set_graph_color(SPEED_MAX, 'darkgreen');
	set_graph_color(IN_SPEED_MAX, 'darkgreen');

	// driving power; datasets must be sorted in stacking order!
	set_graph_color(MUSCLE, 'rgba(255, 170, 0, 1)', 'origin');
	set_graph_color(IN_MUSCLE, 'rgba(255, 170, 0, 1)');
	set_graph_color(DOWN, 'rgba(255, 0, 255, 1)', MUSCLE);
	set_graph_color(DECEL, 'rgba(255, 0, 0, 1)', DOWN);

	// resisting power; datasets must be sorted in stacking order!
	set_graph_color(ROLL, 'rgba(127, 0, 255, 1)', 'origin');
	set_graph_color(AIR, 'rgba(0, 0, 255, 1)', ROLL);
	set_graph_color(UP, 'rgba(0, 127, 255, 1)', AIR);
	set_graph_color(ACCEL, 'rgba(0, 255, 255, 1)', UP);

	// brake
	set_graph_color(BRAKE, 'rgba(0, 255, 128, 1)', ACCEL);


	// diagram line settings
	data.datasets.forEach( function(v,i) {

		// make points thick only for datasets that can be dragged
		data.datasets[i].pointRadius = data.datasets[i].dragData ? 5 : 0;

		data.datasets[i].pointHoverBorderWidth = data.datasets[i].borderWidth;
		data.datasets[i].pointHoverRadius = data.datasets[i].pointRadius;

		// for solid lines, set background color to line color
		// otherwise to a semi-transparent version
		// except MUSCLE: set it to completely transparent
		if (i === MUSCLE)
			data.datasets[i].backgroundColor = 'rgba(0, 0, 0, 0)';
		else if (data.datasets[i].borderWidth > 3)
			data.datasets[i].backgroundColor = data.datasets[i].borderColor;
		else
			data.datasets[i].backgroundColor = data.datasets[i].borderColor.replace(/,[0-9. ]+\)/, ',0.4)');
	} );

	// show parameters in info window
	for (k in params) {
		if (document.getElementById(k))
			document.getElementById(k).value = params[k];
	}

	let ctx = document.getElementById('chart_canvas').getContext('2d');

	canvas_width = Math.min(Math.max(1000 * params['distance'], window.innerWidth), MAX_CANVAS_WIDTH);

	// maximize: scale diagram to window size
	document.getElementById('wrapper').style.width = canvas_width + 'px';
	ctx.canvas.height = document.documentElement.clientHeight;

	// create diagram
	myChart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: {
			responsive: true,
			normalized: true,
			animation: false,		// much faster without animation
			interaction: {
				mode: 'x'		// show all elements in tooltip with same index, instead of only the one below the mouse
			},
			elements: { line: { tension: 0 } },	// straight lines instead of Bézier (0.2) = faster
			pointHitRadius: 25,	// radius around points that react to mouse/touch events
			maintainAspectRatio: false,
			stacked: false,		// do not stack graphs, unless explicitly demanded
			events: ['mousemove', 'mouseout', 'click'],
			onClick: (e) => {

				// react only if the nearest dataset is one of those that can be dragged and is visible (= the corresponding input dataset is visible)
				const nearest_elements = myChart

					// get all datasets at the current x coordinate
					.getElementsAtEventForMode(e, 'x', {}, true)

					// add some useful properties required for filtering and sorting
					.map(v => {
						// 1 = input, 2 = interpolated input, 0 = other
						const type = DATASET_INPUT.indexOf(v.datasetIndex) !== -1 ? 1 : DATASET_INPUT_INTERPOLATED.indexOf(v.datasetIndex) !== -1 ? 2 : 0;
						return Object.assign({
							'datasetType': type,
							'isHidden': data.datasets[type === 2 ? v.datasetIndex + 1 : v.datasetIndex].hidden === true,
							'clickDistance': Math.sqrt((v.element.x - e.x)**2 + (v.element.y - e.y)**2)
						}, v);
					})

					// use only input datasets that are not hidden
					.filter(v => v.datasetType > 0 && !v.isHidden)

					// sort by distance, and if closer than 20 pixel, prefer existing input points (= where type is DATASET_INPUT)
					.sort((a, b) => a.datasetType !== b.datasetType && a.clickDistance < 20 && b.clickDistance < 20 ? a.datasetType - b.datasetType : a.clickDistance - b.clickDistance);

				// if none of the relevant datasets has been found, return
				if (nearest_elements.length === 0)
					return;

				const nearest_dataset = nearest_elements[0].datasetIndex;

				debug_log(['nearest input', nearest_elements.filter(v => DATASET_INPUT.indexOf(v) !== -1)], 2);
				debug_log(['nearest interpolared', nearest_elements.filter(v => DATASET_INPUT_INTERPOLATED.indexOf(v) !== -1)], 2);

				// if it is a non-interpolated datapoint, delete it
				if (DATASET_INPUT.indexOf(nearest_dataset) !== -1) {

					const where = data.datasets[nearest_dataset].data.findIndex( (v) => v.x === nearest_elements[0].element.parsed.x && v.y === nearest_elements[0].element.parsed.y );

					// if we found a matching array element (index != -1), delete it;
					// but skip the first and last element of the array
					if (where > 0 && where < data.datasets[nearest_dataset].data.length - 1) {
						data.datasets[nearest_dataset].data.splice(where, 1);
						debug_log(['delete point', where, data.datasets[nearest_dataset].data], 2);
						debug_log(data.datasets[nearest_dataset], 2);
					}

				}
				// if it is one of the interpolated datasets, add a point to the non-interpolated dataset there
				else if (DATASET_INPUT_INTERPOLATED.indexOf(nearest_dataset) !== -1) {

					// check for the interpolated versions (=> next index), since the input datasets are invisible
					const canvasPosition = Chart.helpers.getRelativePosition(e, myChart);
					const x = myChart.scales.x.getValueForPixel(canvasPosition.x);
					const y = myChart.scales[data.datasets[nearest_dataset + 1].yAxisID].getValueForPixel(canvasPosition.y);
					const where = data.datasets[nearest_dataset + 1].data.findIndex( (v, i, a) => (x >= a[Math.max(i-1, 0)].x && x <= a[i].x));

					// insert new datapoint
					data.datasets[nearest_dataset + 1].data.splice(where, 0, { 'x': x, 'y': y });

					debug_log(['insert point', data.datasets[nearest_dataset + 1].data], 2);
				}

				// recompute everything
				show_spinner(function() {
					compute_diagram();
					update_legend();
					myChart.update();
				});
			},
			plugins: {
				legend: {
					display: false,		// do not display normal legend; we create our own
				},
				title: {
					display: false,
					text: 'GPS Power data'
				},
				tooltip: {
					position: 'nearest',	// show tooltip at nearest datapoint
					intersect: false,	// show the tooltip not only when the mouse is near a line
					titleFont: { size: 15 },
					bodyFont: { size: 15 },
					titleSpacing: 10,
					bodySpacing: 7,
					footerMarginTop: 25,
					padding: 15,
					filter: (v, i, a) => {
						const xpositions = Object.keys(Object.fromEntries(a.map(v => [v.element.x, 1])));
						const medianpos = xpositions[Math.floor(xpositions.length / 2)];
						return v.element.x == medianpos;
					},
					callbacks: {
						title: function(tooltipItem) {

							let index = tooltipItem[0].dataIndex;

							// update map only if it is visible
							if (document.getElementById('map').offsetParent !== null)
							{
								// if position is broken, don't care
								try {
									// find the two neighboring elements
									let [v1, v2] = data['pos'].filter( (v, i, a) => (tooltipItem[0].element.parsed.x >= a[Math.max(i-1, 0)].x && tooltipItem[0].element.parsed.x <= a[i].x) || (tooltipItem[0].element.parsed.x >= a[i].x && tooltipItem[0].element.parsed.x <= a[Math.min(i+1, data['pos'].length-1)].x));

									// interpolate both of their coordinates
									let pos_interpolated = [(tooltipItem[0].element.parsed.x - v1.x) / (v2.x - v1.x) * v2.y[0] + (v2.x - tooltipItem[0].element.parsed.x) / (v2.x - v1.x) * v1.y[0], (tooltipItem[0].element.parsed.x - v1.x) / (v2.x - v1.x) * v2.y[1] + (v2.x - tooltipItem[0].element.parsed.x) / (v2.x - v1.x) * v1.y[1]];

									// move the map marker and map itself to this position
									marker.setLatLng(pos_interpolated);
									map.panTo(pos_interpolated);
								} catch {}

								// if map becomes visible for the first time: recalculate the size
								if (!map_updated) {
									map.invalidateSize();
									map_updated = true;
								}
							}

							return round_to(tooltipItem[0].raw.x, 2) + ' km' + (!isDragging ? ', ⌚ ' + format_time(index) : '');
						},
						labelColor: function(tooltipItem) {
							return {
								borderColor: tooltipItem.dataset.borderColor,
								backgroundColor: tooltipItem.dataset.borderColor,
							};
						},
						label: function(tooltipItem) {

							// show tooltip line only if:
							// * it is not one of the input datasets (because they are only defined in few points)
							// * or during one of the input datapoints is being dragged (=> show its value when dragging)
							// * and it is either one of the interpolated input datasets
							// * or its value is not 0 (or null)
							if ((DATASET_INPUT.indexOf(tooltipItem.datasetIndex) === -1 || isDragging)
								&& (DATASET_INPUT_INTERPOLATED.indexOf(tooltipItem.datasetIndex) !== -1
								|| tooltipItem.raw.y !== 0)
							) {

								// default text: name, value, unit
								text = ' '
									+ tooltipItem.dataset.label	// name of label
									+ ': '
									+ round_to(tooltipItem.raw.y, 1)	// data value, rounded to one digit
									+ ' '
									+ tooltipItem.dataset.unit;	// unit

								if ([DOWN, DECEL, MUSCLE].indexOf(tooltipItem.datasetIndex) !== -1) {
									percentage = tooltipItem.parsed.y * (100 - (tooltipItem.datasetIndex === MUSCLE ? params['dtr'] : 0))
										/ (data.datasets[DOWN].data[tooltipItem.dataIndex].y
											+ data.datasets[DECEL].data[tooltipItem.dataIndex].y
											+ data.datasets[MUSCLE].data[tooltipItem.dataIndex].y * (1 - params['dtr'] / 100));

									text += ' = ' + Math.round(percentage) + '%';
								} else if ([ROLL, AIR, UP, ACCEL, BRAKE].indexOf(tooltipItem.datasetIndex) !== -1) {
									percentage = tooltipItem.parsed.y
										/ (data.datasets[ROLL].data[tooltipItem.dataIndex].y
											+ data.datasets[AIR].data[tooltipItem.dataIndex].y
											+ data.datasets[UP].data[tooltipItem.dataIndex].y
											+ data.datasets[ACCEL].data[tooltipItem.dataIndex].y
											+ data.datasets[BRAKE].data[tooltipItem.dataIndex].y);

									text += ' = ' + format_percent(percentage) + '%';
								}

								return text;
							}
						},
						afterBody: function(tooltipItem) {

							// when not dragging, add the residual power
							if (!isDragging) {
								let P_in = [MUSCLE, DOWN, DECEL].reduce((t, v) => t + (v === MUSCLE ? (1 - params['dtr'] / 100) : 1) * data.datasets[v].data[tooltipItem[0].dataIndex].y, 0);
								let P_out = [ROLL, AIR, UP, ACCEL].reduce((t, v) => t + data.datasets[v].data[tooltipItem[0].dataIndex].y, 0);

								return 'P_resid: ' + round_to(P_in - P_out, 1) + ' W';
							}

							// when dragging the elevation, add the slope from the previous point;
							// since the distance is in km, divide by 10 to get percent
							else if (tooltipItem[0].datasetIndex === IN_ELE && tooltipItem[0].dataIndex > 0) {
								let previous_point = data.datasets[IN_ELE].data[tooltipItem[0].dataIndex - 1];
								return 'slope: ' + round_to( (tooltipItem[0].parsed.y - previous_point.y) / (tooltipItem[0].parsed.x - previous_point.x) / 10, 1) + '%';
							}
						}
					}
				},
				dragData: {
					showTooltip: true,	// show tooltip with current value while dragging
					dragX: true,		// allow dragging horizontally

					onDragStart: (e) => isDragging = true,

					// end dragging: compute distance; if long, move the point; if short, no dragging was intended, delete the point
					onDragEnd: function(e, datasetIndex, index, value) {

						// if at the end of the diagram: allow only vertical movement
						if (index === 0 || index === data.datasets[datasetIndex].data.length - 1) {
							data.datasets[datasetIndex].data[index].y = value.y;
							data.datasets[datasetIndex].data[index].x = index === 0 ? 0 : params['distance'];
						}
						// otherwise: full 2D movement
						else
							data.datasets[datasetIndex].data[index] = value;

						// if a point has been dragged beyond the first element, update this one, because the point is deleted later
						if (index !== 0 && value.x === 0)
							data.datasets[datasetIndex].data[0].y = value.y;

						// sort data according to x coordinate, since the point could have been moved past another point
						data.datasets[datasetIndex].data.sort( (a, b) => a.x - b.x );

						// remove points with duplicate x value => the first point wins
						data.datasets[datasetIndex].data = data.datasets[datasetIndex].data.filter((v, i, arr) => i === 0 || v.x !== arr[i-1].x);

						isDragging = false;
						debug_log(['drag_end', data.datasets[datasetIndex].label], 1);

						show_spinner(function() {
							compute_diagram();
							update_legend();
							myChart.update();
						});
					},
				},
			},
			scales: {
				x: {
					display: true,
					type: 'linear',	// allows interpolation, instead of fixed labels
					grid: { drawOnChartArea: true },
					min: 0,
					max: params['distance'],
					ticks: {
						stepSize: 0.1,		// every 0.1 km
						minRotation: 45,
						maxRotation: 45,
						sampleSize: 10,
						callback: (v, i) => !(i % 5) ? round_to(v, 1) : ' ',
					},
					title: {
						display: false,
						text: 'Distance [km]',
					},
				},
				h: {
					position: 'left',
					display: false,
					grid: { drawOnChartArea: false },
					min: Math.round(0.95 * params['base_ele']),	// make the base elevation not at the very bottom
					max: Math.round(1.1 * params['top_ele']),
					ticks: {
						beginAtZero: false,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: true,
						text: 'Elevation [m]',
					},
				},
				v: {
					position: 'left',
					display: false,
					grid: { drawOnChartArea: true },
					min: 0,
					max: Math.round(1.1 * params['default_vmax']),	// since the speed limit is high, make the axis a bit higher
					ticks: {
						beginAtZero: true,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: true,
						text: 'Speed [km/h]',
					},
				},
				v_wind: {
					position: 'left',
					display: false,
					grid: { drawOnChartArea: true },
					min: -50,
					max: +50,
					ticks: {
						beginAtZero: true,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: true,
						text: 'Speed [km/h]',
					},
				},
				P_in: {
					position: 'left',
					display: true,
					stacked: true,
					grid: { drawOnChartArea: true },
					min: 0,
					max: 1000,
					ticks: {
						beginAtZero: true,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: true,
						text: 'Power [W]',
					},
				},
				P_out: {
					position: 'left',
					display: false,
					stacked: true,
					grid: { drawOnChartArea: false },
					min: 0,
					max: 1000,
					ticks: {
						beginAtZero: true,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: false,
						text: 'Power [W]',
					},
				},
				P_hidden: {
					display: false,
					stacked: false,
					grid: { drawOnChartArea: false },
					min: 0,
					max: 1000,
					ticks: {
						beginAtZero: true,
						minRotation: 0,
						maxRotation: 0,
						sampleSize: 10,
						},
					title: {
						display: false,
						text: 'Power [W]',
					},
				},
			},
			legend: {
				display: false,
			},
		}
	});

	// if a zoom range is given with parameters, use it
	if (params['zoom_min'] || params['zoom_max']) {
		let min = Number(params['zoom_min'] || 0);
		let max = Number(params['zoom_max'] || max_xrange);

		if (min > max)
			[min, max] = [max, min];

		xrange = to_range(max - min, 0, max_xrange);

		zoom_range(min, max);
	}

	// scroll support via Home and End keys, zoom via plus and minus
	document.body.addEventListener('keydown', function(e) {
		if (e.key === 'Home') { window.scrollTo(0,0); }
		else if (e.key === 'End') { window.scrollTo(document.getElementById('chart_canvas').scrollWidth,0); }
		else if (e.key === '+') { zoom(0.8); }
		else if (e.key === '-') { zoom(1.25); }
		else if (e.key === 'ArrowLeft' && xrange < max_xrange && document.scrollingElement.scrollLeft === 0) { scroll_rel(-0.25 * xrange); }
		else if (e.key === 'ArrowRight' && xrange < max_xrange && Math.round(window.pageXOffset) === document.getElementById('chart_canvas').offsetWidth - window.innerWidth) { scroll_rel(+0.25 * xrange); }
	}, true);

	// scroll support via mouse wheel
	document.body.addEventListener('wheel', e => window.scrollBy(Math.sign(e.deltaY) * 100, 0) );

	// zoom support via pinch gesture
	document.body.addEventListener('touchstart', function(e) {
		// two finger touch = pinch
		if (e.touches.length === 2) {
			pinch_start = pinch_dist(e);
		}
		// one finger swipe, only process x component
		else if (e.touches.length === 1) {
			swipe_x_start = e.touches[0].clientX;
		}
	}, true );
	document.body.addEventListener('touchmove', function(e) {
		// two finger touches, update endpoints
		if (e.touches.length === 2) {
			pinch_end = pinch_dist(e)
		}
		// one finger swipe, only process x component
		else if (e.touches.length === 1) {
			swipe_x_end = e.touches[0].clientX;
		}
	}, true );
	document.body.addEventListener('touchend', function(e) {
		// one touch has ended, the other one is still active
		if (e.touches.length === 1 ) {
			e.preventDefault();
			if (pinch_end > 0)
				zoom( pinch_start / pinch_end );
		}
		// no touch active; could have been one-finger swipe
		else if (e.touches.length === 0) {
			// at left end of scroll bar and swiping right
			if (swipe_x_end > swipe_x_start && xrange < max_xrange && document.scrollingElement.scrollLeft === 0) {
				scroll_rel(-0.25 * xrange);
			}
			// at right end of scroll bar and swiping left
			else if (swipe_x_end < swipe_x_start && xrange < max_xrange && Math.round(window.pageXOffset) === document.getElementById('chart_canvas').offsetWidth - window.innerWidth) {
				scroll_rel(+0.25 * xrange);
			}
		}
	}, true );

	// event handlers for all input fields
	[].forEach.call( document.getElementsByTagName('input'), function(v) {

		// prevent scrolling when using the wheel on input number fields
		v.addEventListener('wheel', e => e.stopPropagation() );

		// update diagram with new data
		v.addEventListener('change', function(e) {
			show_spinner(function() {

				// check if the input field belongs to a dropdown list, get the list name
				let selector = v.id.match(/^input_(\w+)_val/);

				// determine the name of the parameter that is modified by this input field
				let param = selector ? document.getElementById('input_' + selector[1] + '_name').options[document.getElementById('input_' + selector[1] + '_name').selectedIndex].value : v.id;

				// assign the new value (and save the old one)
				let prev_param = params[param];
				params[param] = Number(v.value);

				// if the distance is changed, adjust the whole diagram
				if (param === 'distance') {

					// adjust the maximum values of the input datasets, and remove points outside
					DATASET_INPUT.forEach( function(w) {
						data.datasets[w].data[data.datasets[w].data.length - 1].x = params[param];
						data.datasets[w].data = data.datasets[w].data.filter(u => u.x <= params[param]);
					});

					// adjust x axis
					max_xrange = params[param];
					xrange = to_range(xrange, xrange, max_xrange);
					myChart.scales.x.max = params[param];

					// adjust the diagram canvas
					canvas_width = Math.min(Math.max(1000 * params['distance'], window.innerWidth), MAX_CANVAS_WIDTH);
					document.getElementById('wrapper').style.width = canvas_width + 'px';
				}
				else if (param === 'base_ele') {

					// adjust all elevation values to new base elevation (subtract the previous one, add the new one)
					data.datasets[IN_ELE].data.forEach( v => v.y += params['base_ele'] - prev_param );

					// adjust h axis
					myChart.scales.h.min = Math.round(0.95 * params['base_ele']);
				}
				else if (param === 'top_ele') {

					// adjust h axis
					myChart.scales.h.max = Math.round(1.1 * params['top_ele']);
				}

				compute_diagram();
				myChart.update();
				update_legend();
				set_URL_parameters();
			});
		} );
	} );

	// properties selector: if a different property is selected, adjust the unit, min/max/step, tooltip, and fill in current value
	['roll', 'mass', 'other'].forEach( function(v) {
		document.getElementById('input_' + v + '_name').addEventListener('change', function(e) {
			let index = document.getElementById('input_' + v + '_name').selectedIndex;
			let dataset = document.getElementById('input_' + v + '_name').options[index].dataset;

			document.getElementById('input_' + v + '_unit').innerHTML = '&nbsp;' + dataset.unit;
			document.getElementById('input_' + v + '_val').min = dataset.min;
			document.getElementById('input_' + v + '_val').max = dataset.max;
			document.getElementById('input_' + v + '_val').step = dataset.step;
			document.getElementById('input_' + v + '_name').title = dataset.title;
			document.getElementById('input_' + v + '_val').value = params[document.getElementById('input_' + v + '_name').options[index].value];
		} );
		document.getElementById('input_' + v + '_name').dispatchEvent(new Event('change'));
	});

	let legend = document.getElementById('chartjsLegend');

	legend.style.fontFamily = Chart.defaults.font.family;
	legend.style.fontSize = Chart.defaults.font.size + 'pt';
	legend.style.fontStyle = Chart.defaults.font.style;
	legend.style.fontColor = Chart.defaults.color;

	legend.appendChild(document.createElement('ul'));

	let legendItems = document.createDocumentFragment();

	[ELE, SPEED, SPEED_MAX, WIND, MUSCLE, DOWN, DECEL, ROLL, AIR, UP, ACCEL, BRAKE].forEach( function(v, i) {

		debug_log(['added to legend', v, data.datasets[v].label], 1);

		let element = document.createElement('li');

		// set tooltip of dataset
		element.title = data.datasets[v].description;

		// set background color of legend
		element.style.backgroundColor = legend_bg_colors[v];
		element.style.opacity = data.datasets[v].hidden ? 0.4 : 1;

		let colorbox = document.createElement('span');
		colorbox.style.backgroundColor = data.datasets[v].borderColor;
		element.appendChild(colorbox);
		element.appendChild(document.createTextNode(data.datasets[v].label));

		// click: hide/show dataset, adjust legend background color
		element.addEventListener('click', (function(index, legendIndex) {
			return function() {

				let multi_switch = [];
				[[ELE, IN_ELE], [MUSCLE, IN_MUSCLE], [SPEED_MAX, IN_SPEED_MAX], [WIND, IN_WIND]].forEach( v => multi_switch[v[0]] = v[1]);

				// if it is an (invisible) interpolated input dataset, switch the corresponding visible input dataset
				const ds = data.datasets[multi_switch[index] ? index + 1 : index];

				debug_log(['legend clicked', index, ds.label], 1);

				ds.hidden = !ds.hidden;
				legendItems[legendIndex].style.opacity = ds.hidden ? 0.4 : 1;

				show_spinner(function() {
					myChart.update();
					set_URL_parameters();
				});
			};
		})(v, i), false);

		// set labels of output power
		if ([ROLL, AIR, UP, BRAKE].indexOf(v) !== -1) {
			let el = document.getElementById('sum1_' + data.datasets[v].label);
			el.innerHTML = data.datasets[v].label + ':';
			el.title = data.datasets[v].description + '; bezogen auf gesamte Output-Leistung';
			el.nextSibling.innerHTML = '<span id=\"pc1_' + v + '\"></span>%';
			el.style.backgroundColor = legend_bg_colors[v];
			el.nextSibling.style.backgroundColor = legend_bg_colors[v];
		}

		// set labels of input power
		if ([MUSCLE, DOWN].indexOf(v) !== -1) {
			let el = document.getElementById('sum2_' + data.datasets[v].label);
			el.innerHTML = data.datasets[v].label + ':';
			el.title = data.datasets[v].description + (v === MUSCLE ? ' (ohne Antriebsverluste)' : '') + '; bezogen auf gesamte Input-Leistung';
			el.nextSibling.innerHTML = '<span id=\"pc2_' + v + '\"></span>%';
			el.style.backgroundColor = legend_bg_colors[v];
			el.nextSibling.style.backgroundColor = legend_bg_colors[v];
		}

		// set labels of instantaneous power
		if ([ROLL, AIR, UP, ACCEL].indexOf(v) !== -1) {
			let el = document.getElementById('sum3_' + data.datasets[v].label);
			el.innerHTML = data.datasets[v].label + ':';
			el.title = data.datasets[v].description + '; bezogen auf die Leistung während des Tretens';
			el.nextSibling.innerHTML = '<span id=\"pc3_' + v + '\"></span>%';
		}

		legendItems.appendChild(element);
	});

	legend.childNodes[0].appendChild(legendItems);
	legendItems = legend.getElementsByTagName('li');

	// initialize the Leaflet map in the parameters window, but set its size only when it is used for the first time
	create_map();

	// update the statistics created by compute_summary()
	update_legend();

	// set the font properties to the default values of Chart.js
	document.getElementById('chartjsInfo').style.fontFamily = Chart.defaults.font.family;
	document.getElementById('chartjsInfo').style.fontSize = Chart.defaults.font.size + 'pt';
	document.getElementById('chartjsInfo').style.fontStyle = Chart.defaults.font.style;
	document.getElementById('chartjsInfo').style.fontColor = Chart.defaults.color;

	// initialization is finished, switch off spinner
	spinner.display = 'none';
}
</script>
</body>
</html>
